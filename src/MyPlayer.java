import java.util.*;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();
        ArrayList<int[]> list = new ArrayList<int[]>();
        int[] layout1 = {1, 1, 1, 0};
        list.add(layout1);
        int[] layout2 = {0, 1, 1, 1};
        list.add(layout2);
        int[] layout3 = {1, 1, 0, 1};
        list.add(layout3);
        int[] layout4 = {1, 0, 1, 1};
        list.add(layout4);
        int[] layout5 = {2, 2, 2, 0};
        list.add(layout5);
        int[] layout6 = {0, 2, 2, 2};
        list.add(layout6);
        int[] layout7 = {2, 0, 2, 2};
        list.add(layout7);
        int[] layout8 = {2, 2, 0, 2};
        list.add(layout8);
        Move bestMove = search(gameBoard, 6, this.playerNumber, list);
        int[][] board = gameBoard.getBoard();
        //Plays either in the middle if first for atop of the middle if the other player put it there
        if ((board[0][3] == 0 || board[0][3] != this.playerNumber) && board[1][3] == 0) {
            bestMove.move = 3;
            bestMove.value = .5;
        }
        System.out.println(bestMove.value);

        long diff = System.nanoTime() - start;
        double elapsed = (double) diff / 1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber, ArrayList<int[]> layoutList) {
        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i = 0; i < Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber, layoutList);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth - 1, (playerNumber == 1 ? 2 : 1), layoutList);
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber, ArrayList<int[]> layoutList) {
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0
        //Checks each column to see how close a player is to connecting 4 pieces
        int[][] board = gameBoard.getBoard();
        double playerScore = 0;
        //Gives some points to the players if they have pieces in the middle
        for (int i = 1; i < 5; i++) {
            for (int j = 0; j < 4; j++) {
                if (board[j][i] != 0) {
                    playerScore += .5;
                }
            }
        }
        double[] scores = layoutCheck(board, layoutList);
        double player1Score = scores[0];
        double player2Score = scores[1];
        if (playerNumber == 1) {
            player1Score += playerScore;
        } else {
            player2Score += playerScore;
        }
        if (player1Score != 0 && player2Score != 0) {
            if (playerNumber == 2) {
                playerScore = (player2Score - player1Score) / 100;
            } else {
                playerScore = (player1Score - player2Score) / 100;
            }
        }
        if (playerScore > 1) {
            playerScore = playerScore / 10;
        }
        return playerScore;

    }


    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);
        ArrayList<Move> bestMoves = new ArrayList<Move>();
        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }
        for (Move bestValue : moves) {
            if (bestValue.value == max) {
                bestMoves.add(bestValue);
            }
        }
        if (moves.size() > 1) {
            int randomBest = rand.nextInt(bestMoves.size());
            bestMove = bestMoves.get(randomBest);
        }
        return bestMove;
    }

    /**
     * Checks the layout of the board and counts how many groups of three pieces each player has
     * @param board The possible board layout
     * @param layoutList List of possible three in a row layouts
     * @return double[] Amount of three in a rows each player has [0] being player 1 and [1] being player 2
     */
    private static double[] layoutCheck(int[][] board, ArrayList<int[]> layoutList) {
        double[] scores = {0, 0};
        int[] layout = new int[7];
        int[] verticalLayout = new int[7];
        int[] layoutDiagonal = new int[4];
        int[] layoutDiagonal2 = new int[5];
        int[] layoutDiagonal3 = new int[6];
        int[] layoutDiagonal4 = new int[7];
        int[] layoutCheck;
        int k = 0;
        while (k < Board.BOARD_SIZE) {
            for (int i = 0; i < Board.BOARD_SIZE; i++) {
                layout[i] = board[i][k];
                verticalLayout[i] = board[k][i];
            }
            for (int j = 0; j < 4; j++) {
                layoutCheck = layoutList.get(j);
                if (checkForThree(layout, layoutCheck)) {
                    scores[0]++;
                }
                if (checkForThree(verticalLayout, layoutCheck)) {
                    scores[0]++;
                }
            }
            for (int j = 5; j < layoutList.size(); j++) {
                layoutCheck = layoutList.get(j);
                if (checkForThree(layout, layoutCheck)) {
                    scores[1]++;
                }
                if (checkForThree(verticalLayout, layoutCheck)) {
                    scores[1]++;
                }
            }
            k++;
        }
        k = 0;
        while (k < 2) {
            if (k == 0) {
                for (int i = 3, j = 0; i < 7; i++, j++) {
                    layoutDiagonal[j] = board[i][j];
                }
                for (int i = 2, j = 0; i < 7; i++, j++) {
                    layoutDiagonal2[j] = board[i][j];
                }
                for (int i = 1, j = 0; i < 7; i++, j++) {
                    layoutDiagonal3[j] = board[i][j];
                }
                for (int i = 0, j = 0; i < 7; i++, j++) {
                    layoutDiagonal4[j] = board[i][j];
                }
            } else if (k == 1) {
                for (int i = 3, j = 6, l = 0; i < 7; i++, j--, l++) {
                    layoutDiagonal[l] = board[i][j];
                }
                for (int i = 2, j = 6, l = 0; i < 7; i++, j--, l++) {
                    layoutDiagonal2[l] = board[i][j];
                }
                for (int i = 1, j = 6, l = 0; i < 7; i++, j--, l++) {
                    layoutDiagonal3[l] = board[i][j];
                }
                for (int i = 0, j = 6, l = 0; i < 7; i++, j--, l++) {
                    layoutDiagonal4[l] = board[i][j];
                }
            }
            for (int i = 0; i < 4; i++) {
                layoutCheck = layoutList.get(i);
                if (checkForThree(layoutDiagonal, layoutCheck)) {
                    scores[0]++;
                }
                if (checkForThree(layoutDiagonal2, layoutCheck)) {
                    scores[0]++;
                }
                if (checkForThree(layoutDiagonal3, layoutCheck)) {
                    scores[0]++;
                }
                if (checkForThree(layoutDiagonal4, layoutCheck)) {
                    scores[0]++;
                }
            }
            for (int i = 4; i < layoutList.size(); i++) {
                layoutCheck = layoutList.get(i);
                if (checkForThree(layoutDiagonal, layoutCheck)) {
                    scores[1]++;
                }
                if (checkForThree(layoutDiagonal2, layoutCheck)) {
                    scores[1]++;
                }
                if (checkForThree(layoutDiagonal3, layoutCheck)) {
                    scores[1]++;
                }
                if (checkForThree(layoutDiagonal4, layoutCheck)){
                    scores[1]++;
                }
            }
            k++;
        }
        return scores;
    }

    /**
     * Checks if a row, column, or diagonal has a three in a row in it
     * @param layout layout of row, column, or diagonal
     * @param threeCheck layout of three in a row that is being checked
     * @return boolean of if there is a three in a row or not
     */
    private static boolean checkForThree(int[] layout, int[] threeCheck) {
        int j = 0;
        for (int i = 0; i < layout.length; i++) {
            if (j < threeCheck.length && threeCheck[j] == layout[i]) {
                j++;
            } else if (j == threeCheck.length) {
                break;
            } else {
                j = 0;
            }
        }
        return j == threeCheck.length;
    }
}